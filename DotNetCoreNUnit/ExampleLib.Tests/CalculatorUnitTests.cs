﻿using NUnit.Framework;
using FluentAssertions;
using ExampleLib;

namespace ExampleLib.Tests
{
    [TestFixture]
    public class CalculatorUnitTests
    {
        [TestCase(1, 1, 2)]
        [TestCase(1.5, 2.5, 4.0)]
        [TestCase(0, 0, 0)]
        public void Add_AddsTheNumbers(decimal x, decimal y, decimal expectedResult)
        {
            Calculator.Add(x, y).Should().Be(expectedResult);
        }
    }
}